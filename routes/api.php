<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth', 'namespace' => 'API'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user-detail', 'AuthController@user');
        Route::post('update-profil/{user}', 'AuthController@updateProfil');
    });
});


Route::group(['middleware' => 'auth:api', 'namespace' => 'API'], function() {
    Route::get('read-all-users', 'UserController@index');
    Route::get('read-user-byId/{user}', 'UserController@UserById');
    Route::post('create-user', 'UserController@store');
    Route::post('update-user/{user}', 'UserController@update');
    Route::get('delete-user/{user}', 'UserController@delete');

    Route::get('read-all-userBalance', 'UserBalanceController@index');
    Route::get('read-userBalance-byId/{userbalance}', 'UserBalanceController@ById');
    Route::post('create-userBalance', 'UserBalanceController@store');
    Route::post('update-userBalance/{userbalance}', 'UserBalanceController@update');
    Route::get('delete-userBalance/{userbalance}', 'UserBalanceController@delete');

    Route::get('balanceBank', 'BalanceBankController@index');
    Route::get('balanceBank/{balancebank}', 'BalanceBankController@ById');
    Route::post('balanceBank/create', 'BalanceBankController@store');
    Route::post('balanceBank/update/{balancebank}', 'BalanceBankController@update');
    Route::get('balanceBank/delete/{balancebank}', 'BalanceBankController@delete');

    Route::get('history/balanceBank', 'BankBalanceHistoryController@index');
    Route::get('history/balanceBank/{bankbalancehistory}', 'BankBalanceHistoryController@ById');
    Route::post('history/balanceBank/create', 'BankBalanceHistoryController@store');
    Route::post('history/balanceBank/update/{bankbalancehistory}', 'BankBalanceHistoryController@update');
    Route::get('history/balanceBank/delete/{bankbalancehistory}', 'BankBalanceHistoryController@delete');

    Route::get('history/userBalance', 'UserBalanceHistoryController@index');
    Route::get('history/userBalance/{userbalancehistory}', 'UserBalanceHistoryController@ById');
    Route::post('history/userBalance/create', 'UserBalanceHistoryController@store');
    Route::post('history/userBalance/update/{userbalancehistory}', 'UserBalanceHistoryController@update');
    Route::get('history/userBalance/delete/{userbalancehistory}', 'UserBalanceHistoryController@delete');

    Route::post('topup', 'TopupController@doTopup');
    Route::get('checkSaldoUser', 'TopupController@checkSaldo');
    Route::get('userbalance/history', 'TopupController@history');

    Route::post('transfer', 'TransferController@doTransfer');
    Route::get('checkSaldoBank', 'TransferController@checkSaldo');
    Route::get('bankbalance/history', 'TransferController@history');
});


