<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BankBalanceHistory extends Model
{
    protected $fillable = [
        'balance_before',
        'balance_after',
        'activity',
        'type',
        'ip',
        'location',
        'user_agent',
        'author'
    ];

    /**
     * @return BelongsTo
     */
    public function balancebank(){
        return $this->belongsTo(BalanceBank::class, 'balance_bank_id');
    }
}
