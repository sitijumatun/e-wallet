<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserBalanceHistory extends Model
{
    protected $fillable = [
        'balance_before',
        'balance_after',
        'activity',
        'type',
        'ip',
        'location',
        'user_agent',
        'author'
    ];

    /**
     * @return BelongsTo
     */
    public function userbalance(){
        return $this->belongsTo(UserBalance::class, 'user_balance_id');
    }
}
