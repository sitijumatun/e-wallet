<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class BalanceBank extends Model
{
    protected $fillable = [
        'balance',
        'balance_achieve',
        'code',
        'enable'
    ];

    /**
     * @return HasOne
     */
    public function history(){
        return $this->hasOne(BankBalanceHistory::class);
    }
}
