<?php

namespace App\Http\Controllers\API;

use App\Exceptions\APIException;
use App\Transformers\UserTransformer;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use App\Exceptions\ValidationException as FailedEntityException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws FailedEntityException|AuthenticationException
     */
    public function login(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|string|email',
                'password' => 'required|string'
            ]);
        } catch (ValidationException $e) {
            throw new FailedEntityException($e->getMessage());
        }

        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)) {
            throw new AuthenticationException('Unauthorized!');
        }

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Login Success!',
            'payload' => fractal($request->user(), new UserTransformer()),
        ],200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws FailedEntityException
     * @throws APIException
     */
    public function signup(Request $request){
        try {
            $this->validate($request, [
                'username' => 'required|string',
                'email' => 'required|string|email|unique:users',
                'password' => 'required|string|confirmed'
            ]);
        } catch (ValidationException $e) {
            throw new FailedEntityException($e->getMessage());
        }

        $user = new User();
        $user->fill($request->all());
        $user->password = bcrypt($request->get('password'));
        if(!$user->save()){
            throw new APIException('Saving Data Failed!');
        }

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Register Success!',
            'payload' => fractal($user, new UserTransformer()),
        ],200);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return JsonResponse [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Logout Success!',
            'payload' => [],
        ],200);
    }

    /**
     * Get the authenticated User
     *
     * @param Request $request
     * @return JsonResponse [json] user object
     */
    public function user(Request $request)
    {
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'User Detail Retrieved!',
            'payload' => fractal($request->user(), new UserTransformer()),
        ],200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws APIException
     * @throws FailedEntityException
     */
    public function updateProfil(Request $request){
        try {
            $this->validate($request, [
                'username' => 'required|string',
                'email' => 'required|string|email',
            ]);
        } catch (ValidationException $e) {
            throw new FailedEntityException($e->getMessage());
        }

        $user = $request->user();
        $user->fill($request->all());
        if($request->has('password')) {
            $user->password = bcrypt($request->get('password'));
        }
        if(!$user->update()){
            throw new APIException('Updating Data Failed!');
        }

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Update Profil Success!',
            'payload' => fractal($user, new UserTransformer()),
        ],200);
    }

}
