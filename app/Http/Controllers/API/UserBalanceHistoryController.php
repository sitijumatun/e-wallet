<?php

namespace App\Http\Controllers\API;

use App\BankBalanceHistory;
use App\Transformers\UserBalanceHistoryTransformer;
use App\UserBalanceHistory;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class UserBalanceHistoryController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(){
        $userbalancehistories = UserBalanceHistory::orderBy('created_at')->get();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Retrieved!',
            'payload' => fractal($userbalancehistories, new UserBalanceHistoryTransformer()),
        ],200);
    }

    /**
     * @param UserBalanceHistory $userbalancehistory
     * @return JsonResponse
     */
    public function byId(UserBalanceHistory $userbalancehistory){
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Retrieved!',
            'payload' => fractal($userbalancehistory, new UserBalanceHistoryTransformer()),
        ],200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \App\Exceptions\ValidationException
     */
    public function store(Request $request){
        try {
            $this->validate($request, [
                'balance_before' => 'required',
                'balance_after' => 'required',
                'activity' => 'required',
                'type' => 'required',
                'ip' => 'required',
                'location' => 'required',
                'user_agent' => 'required',
                'author' => 'required',
                'user_balance_id' => 'required'
            ]);
        } catch (ValidationException $e) {
            throw new \App\Exceptions\ValidationException($e->getMessage());
        }

        $userbalancehistory = new UserBalanceHistory();
        $userbalancehistory->fill($request->all());
        $userbalancehistory->userbalance()->associate($request->get('user_balance_id'));
        $userbalancehistory->save();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data saved!',
            'payload' => fractal($userbalancehistory, new UserBalanceHistoryTransformer()),
        ],200);
    }

    /**
     * @param Request $request
     * @param UserBalanceHistory $userbalancehistory
     * @return JsonResponse
     * @throws \App\Exceptions\ValidationException
     */
    public function update(Request $request, UserBalanceHistory $userbalancehistory){
        try {
            $this->validate($request, [
                'balance_before' => 'required',
                'balance_after' => 'required',
                'activity' => 'required',
                'type' => 'required',
                'ip' => 'required',
                'location' => 'required',
                'user_agent' => 'required',
                'author' => 'required',
                'user_balance_id' => 'required'
            ]);
        } catch (ValidationException $e) {
            throw new \App\Exceptions\ValidationException($e->getMessage());
        }

        $userbalancehistory->fill($request->all());
        $userbalancehistory->userbalance()->associate($request->get('user_balance_id'));
        $userbalancehistory->update();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Updated!',
            'payload' => fractal($userbalancehistory, new UserBalanceHistoryTransformer()),
        ],200);
    }

    /**
     * @param UserBalanceHistory $userbalancehistory
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(UserBalanceHistory $userbalancehistory){
        $userbalancehistory->delete();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Delete!',
            'payload' => [],
        ],200);
    }


}
