<?php

namespace App\Http\Controllers\API;

use App\BalanceBank;
use App\Constants\ActivityType;
use App\Exceptions\APIException;
use App\Services\BankService;
use App\Services\UserService;
use App\Transformers\BalanceBankTransformer;
use App\Transformers\UserBalanceHistoryTransformer;
use App\Transformers\UserBalanceTransformer;
use App\User;
use App\UserBalance;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class TopupController extends Controller
{
    public $userService;
    public $bankService;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->bankService = new BankService();
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws APIException
     * @throws \App\Exceptions\ValidationException
     */
    public function doTopup(Request $request){
        try {
            $this->validate($request, [
                'topup_amount' => 'required',
                'detail.activity' => 'required',
                'detail.ip' => 'required',
                'detail.location' => 'required',
                'detail.user_agent' => 'required',
                'detail.author' => 'required'
            ]);
        } catch (ValidationException $e) {
            throw new \App\Exceptions\ValidationException($e->getMessage());
        }

        $balancebank = BalanceBank::orderBy('created_at','desc')->first();
        if(!$balancebank){
            throw new APIException('Balance Bank Not Found!');
        }

        if($balancebank->enable === false){
            throw new APIException('Sorry bank not available right now!');
        }

        if((int)$balancebank->balance < (int)$request->get('topup_amount')){
            throw new APIException('Sorry Your Balance Not Enough!');
        }

        $fillHistory = [
            'activity' => $request['detail']['activity'],
            'ip'=> $request['detail']['ip'],
            'location' => $request['detail']['location'],
            'user_agent' => $request['detail']['user_agent'],
            'author' => $request['detail']['author']
        ];


        $balanceBeforeUser = (int)$this->userService->lastBalance($request->user());
        $balanceAfterUser = $balanceBeforeUser + (int)$request->get('topup_amount');
        $fillUserBalance = [
            'balance_achieve' => $request->get('topup_amount'),
            'balance' => $balanceAfterUser
        ];

        $fillUserHistory = $fillHistory +=[
            'balance_before' => $balanceBeforeUser,
            'balance_after'=> $balanceAfterUser,
            'type' => ActivityType::DEBIT,
        ];


        $balanceBeforeBank = (int)$this->bankService->lastBalance();
        $balanceAfterBank = $balanceBeforeBank - (int)$request->get('topup_amount');
        $fillBankBalance = [
            'balance_achieve' => $request->get('topup_amount'),
            'balance' => $balanceAfterBank,
            'code' => $balancebank->code,
            'enable' => $balancebank->enable
        ];

        $fillBankHistory = $fillHistory +=[
            'balance_before' => $balanceBeforeBank,
            'balance_after'=> $balanceAfterBank,
            'type' => ActivityType::CREDIT,
        ];

        try {
            DB::beginTransaction();

            $userBalance = $this->userService->createUserBalance($fillUserBalance, $request->user());
            $historyUser = $this->userService->createHistoryUserBalance($fillUserHistory,$userBalance);

            $bankBalance = $this->bankService->createBankBalance($fillBankBalance);
            $historyBank = $this->bankService->createHistoryBankBalance($fillBankHistory,$bankBalance);

            DB::commit();

            return response()->json([
                'code' => 200,
                'success' => true,
                'message' => 'Last Balance Retrieved !',
                'payload' => [
                    'pencatatanUser' => fractal($userBalance, new UserBalanceTransformer()),
                    'pencatatanBank' => fractal($bankBalance, new BalanceBankTransformer())
                ]
            ],200);



        }catch (\Exception $e){
            DB::rollBack();
            throw new APIException($e->getMessage());
        }


    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function checkSaldo(Request $request){
        $saldo = $this->userService->lastBalance($request->user());

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Last Balance Retrieved !',
            'payload' => $saldo,
        ],200);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function history(Request $request){
        $histories = $this->userService->history($request->user());

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'History Retrieved!',
            'payload' => fractal($histories, new UserBalanceHistoryTransformer()),
        ],200);
    }


}
