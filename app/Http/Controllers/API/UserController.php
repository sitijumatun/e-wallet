<?php

namespace App\Http\Controllers\API;

use App\Exceptions\APIException;
use App\Exceptions\ValidationException as FailedEntityException;
use App\Transformers\UserManagementTransformer;
use App\Transformers\UserTransformer;
use App\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(){
        $users = User::orderBy('username')->get();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'User Data Retrieved!',
            'payload' => fractal($users, new UserManagementTransformer()),
        ],200);
    }

    /**
     * @param User $user
     * @return JsonResponse
     */
    public function UserById(User $user){
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'User Data Retrieved!',
            'payload' => fractal($user, new UserManagementTransformer()),
        ],200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws FailedEntityException
     */
    public function store(Request $request){
        try {
            $this->validate($request, [
                'username' => 'required|string',
                'email' => 'required|string|email|unique:users',
                'password' => 'required|string|confirmed'
            ]);
        } catch (ValidationException $e) {
            throw new FailedEntityException($e->getMessage());
        }

        $user = new User();
        $user->fill($request->all());
        $user->password = bcrypt($request->get('password'));
        $user->save();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'User Saved Successfully!',
            'payload' => fractal($user, new UserManagementTransformer()),
        ],200);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     * @throws APIException
     * @throws FailedEntityException
     */
    public function update(Request $request, User $user){
        try {
            $this->validate($request, [
                'username' => 'required|string',
                'email' => 'required|string|email',
            ]);
        } catch (ValidationException $e) {
            throw new FailedEntityException($e->getMessage());
        }

        $user->fill($request->all());
        if($request->has('password')) {
            $user->password = bcrypt($request->get('password'));
        }
        if(!$user->update()){
            throw new APIException('Updating Data Failed!');
        }

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Update Data Success!',
            'payload' => fractal($user, new UserManagementTransformer()),
        ],200);
    }

    /**
     * @param User $user
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(User $user){
        $user->delete();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Delete Data Success!',
            'payload' => [],
        ],200);
    }
}
