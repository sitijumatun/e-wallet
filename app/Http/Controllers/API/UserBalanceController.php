<?php

namespace App\Http\Controllers\API;

use App\Transformers\UserBalanceTransformer;
use App\UserBalance;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class UserBalanceController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request){
        $userBalances = UserBalance::orderBy('created_at', 'desc')->get();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Retrieved!',
            'payload' => fractal($userBalances, new UserBalanceTransformer()),
        ],200);
    }

    /**
     * @param UserBalance $userbalance
     * @return JsonResponse
     */
    public function ById(UserBalance $userbalance){
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Retrieved!',
            'payload' => fractal($userbalance, new UserBalanceTransformer()),
        ],200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \App\Exceptions\ValidationException
     */
    public function store(Request $request){
        try {
            $this->validate($request, [
                'balance' => 'required',
                'balance_achieve' => 'required'
            ]);
        } catch (ValidationException $e) {
            throw new \App\Exceptions\ValidationException($e->getMessage());
        }

        $userbalance = new UserBalance();
        $userbalance->fill($request->all());
        $userbalance->user()->associate($request->user());
        $userbalance->save();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Saved!',
            'payload' => fractal($userbalance, new UserBalanceTransformer()),
        ],200);
    }

    /**
     * @param Request $request
     * @param UserBalance $userbalance
     * @return JsonResponse
     * @throws \App\Exceptions\ValidationException
     */
    public function update(Request $request, UserBalance $userbalance){
        try {
            $this->validate($request, [
                'balance' => 'required',
                'balance_achieve' => 'required'
            ]);
        } catch (ValidationException $e) {
            throw new \App\Exceptions\ValidationException($e->getMessage());
        }

        $userbalance->fill($request->all());
        $userbalance->user()->associate($request->user());
        $userbalance->update();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Updated!',
            'payload' => fractal($userbalance, new UserBalanceTransformer()),
        ],200);
    }

    /**
     * @param UserBalance $userbalance
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(UserBalance $userbalance){
        $userbalance->delete();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Deleted!',
            'payload' => [],
        ],200);
    }



}
