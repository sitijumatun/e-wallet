<?php

namespace App\Http\Controllers\API;

use App\BalanceBank;
use App\Transformers\BalanceBankTransformer;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class BalanceBankController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(){
        $balancebanks = BalanceBank::orderBy('created_at', 'desc')->get();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Retrieved!',
            'payload' => fractal($balancebanks, new BalanceBankTransformer()),
        ],200);
    }

    /**
     * @param BalanceBank $balancebank
     * @return JsonResponse
     */
    public function ById(BalanceBank $balancebank){
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Retrieved!',
            'payload' => fractal($balancebank, new BalanceBankTransformer()),
        ],200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \App\Exceptions\ValidationException
     */
    public function store(Request $request){
        try {
            $this->validate($request, [
                'balance' => 'required',
                'balance_achieve' => 'required',
                'code' => 'required',
                'enable' => 'required'
            ]);
        } catch (ValidationException $e) {
            throw new \App\Exceptions\ValidationException($e->getMessage());
        }

        $balancebank = new BalanceBank();
        $balancebank->fill($request->all());
        $balancebank->save();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Saved!',
            'payload' => fractal($balancebank, new BalanceBankTransformer()),
        ],200);
    }

    /**
     * @param Request $request
     * @param BalanceBank $balancebank
     * @return JsonResponse
     * @throws \App\Exceptions\ValidationException
     */
    public function update(Request $request, BalanceBank $balancebank){
        try {
            $this->validate($request, [
                'balance' => 'required',
                'balance_achieve' => 'required',
                'code' => 'required',
                'enable' => 'required'
            ]);
        } catch (ValidationException $e) {
            throw new \App\Exceptions\ValidationException($e->getMessage());
        }

        $balancebank->fill($request->all());
        $balancebank->update();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Updated!',
            'payload' => fractal($balancebank, new BalanceBankTransformer()),
        ],200);
    }

    /**
     * @param BalanceBank $balancebank
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(BalanceBank $balancebank){
        $balancebank->delete();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Deleted!',
            'payload' => [],
        ],200);
    }
}
