<?php

namespace App\Http\Controllers\API;

use App\Constants\ActivityType;
use App\Exceptions\APIException;
use App\Services\BankService;
use App\Services\UserService;
use App\Transformers\BalanceBankTransformer;
use App\Transformers\BankBalanceHistoryTransformer;
use App\Transformers\UserBalanceTransformer;
use App\UserBalance;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class TransferController extends Controller
{
    public $userService;
    public $bankService;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->bankService = new BankService();
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws APIException
     * @throws \App\Exceptions\ValidationException
     */
    public function doTransfer(Request $request){
        try {
            $this->validate($request, [
                'transfer_amount' => 'required',
                'bank_code'=> 'required',
                'detail.activity' => 'required',
                'detail.ip' => 'required',
                'detail.location' => 'required',
                'detail.user_agent' => 'required',
                'detail.author' => 'required'
            ]);
        } catch (ValidationException $e) {
            throw new \App\Exceptions\ValidationException($e->getMessage());
        }

        $balanceuser = UserBalance::orderBy('created_at', 'desc')->first();
        if(!$balanceuser){
            throw new APIException('Balance User Not Found!');
        }

        if((int)$balanceuser->balance < (int)$request->get('transfer_amount')){
            throw new APIException('Sorry Your Balance Not Enough!');
        }

        $fillHistory = [
            'activity' => $request['detail']['activity'],
            'ip'=> $request['detail']['ip'],
            'location' => $request['detail']['location'],
            'user_agent' => $request['detail']['user_agent'],
            'author' => $request['detail']['author']
        ];

        $balanceBeforeUser = (int)$this->userService->lastBalance($request->user());
        $balanceAfterUser = $balanceBeforeUser - (int)$request->get('transfer_amount');
        $fillUserBalance = [
            'balance_achieve' => $request->get('transfer_amount'),
            'balance' => $balanceAfterUser
        ];

        $fillUserHistory = $fillHistory +=[
            'balance_before' => $balanceBeforeUser,
            'balance_after'=> $balanceAfterUser,
            'type' => ActivityType::CREDIT,
        ];

        $balanceBeforeBank = (int)$this->bankService->lastBalance();
        $balanceAfterBank = $balanceBeforeBank + (int)$request->get('transfer_amount');
        $fillBankBalance = [
            'balance_achieve' => $request->get('transfer_amount'),
            'balance' => $balanceAfterBank,
            'code' => $request->get('bank_code'),
            'enable' => true
        ];

        $fillBankHistory = $fillHistory +=[
            'balance_before' => $balanceBeforeBank,
            'balance_after'=> $balanceAfterBank,
            'type' => ActivityType::DEBIT,
        ];

        try {
            DB::beginTransaction();

            $userBalance = $this->userService->createUserBalance($fillUserBalance, $request->user());
            $historyUser = $this->userService->createHistoryUserBalance($fillUserHistory,$userBalance);

            $bankBalance = $this->bankService->createBankBalance($fillBankBalance);
            $historyBank = $this->bankService->createHistoryBankBalance($fillBankHistory,$bankBalance);

            DB::commit();

            return response()->json([
                'code' => 200,
                'success' => true,
                'message' => 'Last Balance Retrieved !',
                'payload' => [
                    'pencatatanUser' => fractal($userBalance, new UserBalanceTransformer()),
                    'pencatatanBank' => fractal($bankBalance, new BalanceBankTransformer())
                ]
            ],200);

        }catch (\Exception $e){
            DB::rollBack();
            throw new APIException($e->getMessage());
        }


    }


    /**
     * @return JsonResponse
     */
    public function checkSaldo(){
        $saldo = $this->bankService->lastBalance();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Last Balance Retrieved !',
            'payload' => $saldo,
        ],200);

    }

    /**
     * @return JsonResponse
     */
    public function history(){
        $histories = $this->bankService->history();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'History Retrieved!',
            'payload' => fractal($histories, new BankBalanceHistoryTransformer()),
        ],200);
    }
}
