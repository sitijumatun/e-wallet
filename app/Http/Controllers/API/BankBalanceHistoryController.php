<?php

namespace App\Http\Controllers\API;

use App\BankBalanceHistory;
use App\Transformers\BankBalanceHistoryTransformer;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class BankBalanceHistoryController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(){
        $bankbalancehistories = BankBalanceHistory::orderBy('created_at')->get();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Retrieved!',
            'payload' => fractal($bankbalancehistories, new BankBalanceHistoryTransformer()),
        ],200);
    }

    /**
     * @param BankBalanceHistory $bankbalancehistory
     * @return JsonResponse
     */
    public function byId(BankBalanceHistory $bankbalancehistory){
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Retrieved!',
            'payload' => fractal($bankbalancehistory, new BankBalanceHistoryTransformer()),
        ],200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \App\Exceptions\ValidationException
     */
    public function store(Request $request){
        try {
            $this->validate($request, [
                'balance_before' => 'required',
                'balance_after' => 'required',
                'activity' => 'required',
                'type' => 'required',
                'ip' => 'required',
                'location' => 'required',
                'user_agent' => 'required',
                'author' => 'required',
                'balance_bank_id' => 'required'
            ]);
        } catch (ValidationException $e) {
            throw new \App\Exceptions\ValidationException($e->getMessage());
        }

        $bankbalancehistory = new BankBalanceHistory();
        $bankbalancehistory->fill($request->all());
        $bankbalancehistory->balancebank()->associate($request->get('balance_bank_id'));
        $bankbalancehistory->save();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Saved!',
            'payload' => fractal($bankbalancehistory, new BankBalanceHistoryTransformer()),
        ],200);
    }

    /**
     * @param Request $request
     * @param BankBalanceHistory $bankbalancehistory
     * @return JsonResponse
     * @throws \App\Exceptions\ValidationException
     */
    public function update(Request $request, BankBalanceHistory $bankbalancehistory){
        try {
            $this->validate($request, [
                'balance_before' => 'required',
                'balance_after' => 'required',
                'activity' => 'required',
                'type' => 'required',
                'ip' => 'required',
                'location' => 'required',
                'user_agent' => 'required',
                'author' => 'required',
                'balance_bank_id' => 'required'
            ]);
        } catch (ValidationException $e) {
            throw new \App\Exceptions\ValidationException($e->getMessage());
        }

        $bankbalancehistory->fill($request->all());
        $bankbalancehistory->balancebank()->associate($request->get('balance_bank_id'));
        $bankbalancehistory->update();

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Updated!',
            'payload' => fractal($bankbalancehistory, new BankBalanceHistoryTransformer()),
        ],200);
    }

    /**
     * @param BankBalanceHistory $bankbalancehistory
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(BankBalanceHistory $bankbalancehistory){
        $bankbalancehistory->delete();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data Delete!',
            'payload' => [],
        ],200);
    }


}
