<?php

namespace App\Transformers;

use App\UserBalanceHistory;
use League\Fractal\TransformerAbstract;

class UserBalanceHistoryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param UserBalanceHistory $userbalancehistory
     * @return array
     */
    public function transform(UserBalanceHistory $userbalancehistory)
    {
        return [
            'id' => $userbalancehistory->id,
            'balanceBefore' => $userbalancehistory->balance_before,
            'balanceAfter' => $userbalancehistory->balance_after,
            'activity'=> $userbalancehistory->activity,
            'type' => $userbalancehistory->type,
            'ip' => $userbalancehistory->ip,
            'location' => $userbalancehistory->location,
            'userAgent' => $userbalancehistory->user_agent,
            'author' => $userbalancehistory->author,
            'balanceUser' => $userbalancehistory->userbalance
        ];
    }
}
