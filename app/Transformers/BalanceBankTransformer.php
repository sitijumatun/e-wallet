<?php

namespace App\Transformers;

use App\BalanceBank;
use League\Fractal\TransformerAbstract;

class BalanceBankTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param BalanceBank $balancebank
     * @return array
     */
    public function transform(BalanceBank $balancebank)
    {
        return [
            'id' => $balancebank->id,
            'balance' => $balancebank->balance,
            'balanceAchieve' => $balancebank->balance_achieve,
            'code' => $balancebank->code,
            'enable' => $balancebank->enable,
            'historyActivity' => $balancebank->history
        ];
    }
}
