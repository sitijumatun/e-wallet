<?php

namespace App\Transformers;

use App\UserBalance;
use League\Fractal\TransformerAbstract;

class UserBalanceTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param UserBalance $userbalance
     * @return array
     */
    public function transform(UserBalance $userbalance)
    {
        return [
            'id' => $userbalance->id,
            'balance' => $userbalance->balance,
            'balanceAchieve' => $userbalance->balance_achieve,
            'historyActivity' => $userbalance->history
        ];
    }
}
