<?php

namespace App\Transformers;

use App\BankBalanceHistory;
use League\Fractal\TransformerAbstract;

class BankBalanceHistoryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param BankBalanceHistory $bankbalancehistory
     * @return array
     */
    public function transform(BankBalanceHistory $bankbalancehistory)
    {
        return [
            'id' => $bankbalancehistory->id,
            'balanceBefore' => $bankbalancehistory->balance_before,
            'balanceAfter' => $bankbalancehistory->balance_after,
            'activity'=> $bankbalancehistory->activity,
            'type' => $bankbalancehistory->type,
            'ip' => $bankbalancehistory->ip,
            'location' => $bankbalancehistory->location,
            'userAgent' => $bankbalancehistory->user_agent,
            'author' => $bankbalancehistory->author,
            'balanceBank' => $bankbalancehistory->balancebank
        ];
    }
}
