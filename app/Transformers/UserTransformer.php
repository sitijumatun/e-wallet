<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;
use Carbon\Carbon;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $tokenExpired = Carbon::now()->addWeeks(1);
        $token->expires_at = $tokenExpired;
        $token->save();

        return [
            'id' => $user->id,
            'username' => $user->username,
            'email' => $user->email,
            'accessToken' => $tokenResult->accessToken,
            'tokenExpiredAt' =>  Carbon::now()->addWeeks(1)->format('Y-m-d H:i:s')
        ];
    }
}
