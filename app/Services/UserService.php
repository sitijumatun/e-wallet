<?php


namespace App\Services;
use App\User;
use App\UserBalance;
use App\UserBalanceHistory;
use Illuminate\Database\Eloquent\Builder;

class UserService
{
    /**
     * @param User $user
     * @return int
     */
    public function lastBalance(User $user){
        $userBalance = UserBalance::where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->first();
        return $userBalance ? $userBalance->balance : 0;
    }

    /**
     * @param $data
     * @param User $user
     * @return UserBalance
     */
    public function createUserBalance($data, User $user){
        $userbalance = new UserBalance();
        $userbalance->fill($data);
        $userbalance->user()->associate($user);
        $userbalance->save();

        return $userbalance;
    }

    /**
     * @param $data
     * @param UserBalance $userbalance
     * @return UserBalanceHistory
     */
    public function createHistoryUserBalance($data, UserBalance $userbalance){
        $userbalancehistory = new UserBalanceHistory();
        $userbalancehistory->fill($data);
        $userbalancehistory->userbalance()->associate($userbalance);
        $userbalancehistory->save();

        return $userbalancehistory;
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function history(User $user){
        return UserBalanceHistory::orderBy('created_at','desc')
                        ->whereHas('userbalance', static function (Builder $query) use ($user){
                            return $query->where('user_id', $user->id);
                        })
                        ->get();
    }
}
