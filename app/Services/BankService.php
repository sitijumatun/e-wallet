<?php


namespace App\Services;


use App\BalanceBank;
use App\BankBalanceHistory;


class BankService
{
    /**
     * @return int
     */
    public function lastBalance(){
        $bankBalance = BalanceBank::orderBy('created_at', 'desc')
            ->first();
        return $bankBalance ? $bankBalance->balance : 0;
    }

    /**
     * @param $data
     * @return BalanceBank
     */
    public function createBankBalance($data){
        $bankbalance = new BalanceBank();
        $bankbalance->fill($data);
        $bankbalance->save();

        return $bankbalance;
    }

    /**
     * @param $data
     * @param BalanceBank $balancebank
     * @return BankBalanceHistory
     */
    public function createHistoryBankBalance($data, BalanceBank $balancebank){
        $bankbalancehistory = new BankBalanceHistory();
        $bankbalancehistory->fill($data);
        $bankbalancehistory->balancebank()->associate($balancebank);
        $bankbalancehistory->save();

        return $bankbalancehistory;
    }

    /**
     * @return mixed
     */
    public function history(){
        return BankBalanceHistory::orderBy('created_at', 'desc')
                                ->get();
    }

}
