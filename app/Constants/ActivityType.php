<?php


namespace App\Constants;


class ActivityType
{
    public const CREDIT = 'credit';
    public const DEBIT = 'debit';
}
